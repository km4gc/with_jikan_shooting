﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defeat_Effect : MonoBehaviour
{

    private void Start()
    {
        
    }

    private void Update()
    {
        if(gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
        {
            Destroy(gameObject);
        }
    }
}
