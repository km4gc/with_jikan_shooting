﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour{

    public float NormalSpeed = 10f; //通常スピード
    public float SlowSpeed = 3f;    //低速スピード

    public float maxhp;        //HP
    public float hp;
    private Vector2 dir;

    private float boundL;
    private float boundR;
    private float boundT;
    private float boundB;

    private new Rigidbody2D rigidbody2D;

    private bool isBomb;

    private bool gameOver;

    int frame;

    public GameObject BulletPrefab;
    public GameObject BombPrefab;
    public GameObject defeat_anime;
   
    AudioSource audioSource;

    public AudioClip damageClip;
    public AudioClip shotClip;
    public AudioClip bombClip;

    // 敵弾のダメージを格納
    private int damage;

    // Start is called before the first frame update
    private void Start()
    {
        hp = maxhp;
        frame = 0;

        gameOver = false;
        isBomb = true;

        this.rigidbody2D = this.GetComponent<Rigidbody2D>();

        var mainCamera = Camera.main;
        var positionZ = this.transform.position.z;
        var topRight = mainCamera.ViewportToWorldPoint(new Vector3(1.0f, 1.0f, positionZ));
        var bottomLeft = mainCamera.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, positionZ));
        this.boundL = bottomLeft.x;
        this.boundR = topRight.x;
        this.boundT = topRight.y;
        this.boundB = bottomLeft.y;

        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        frame++;

        var x = Input.GetAxisRaw("Horizontal");
        var y = Input.GetAxisRaw("Vertical");

        this.dir = new Vector2(x, y).normalized;
        //生存中はスコア加算
        if (frame % 6 == 0 && hp > 0)
        {
            GameObject.Find("Canvas").GetComponent<UIController>().AddScore();
        }
        //撃破された後のこと
        else if (hp <= 0)
        {
            isGameOver();
        }

        if (gameOver)
        {
            GameObject.Find("Canvas").GetComponent<UIController>().GameOver();
            Destroy(gameObject);
            var Anime = Instantiate(defeat_anime, transform.position, transform.rotation);
        }

        //Zキーで射撃，3フレーム間隔
        if (Input.GetKey(KeyCode.Z) && frame % 3 == 0)
        {
            if(frame % 6 == 0) audioSource.PlayOneShot(shotClip);
            Instantiate(BulletPrefab, transform.position, Quaternion.identity);
        }

        //Xキーでボム
        if (Input.GetKeyDown(KeyCode.X) && isBomb==true)
        {
            audioSource.PlayOneShot(bombClip);
            Instantiate(BombPrefab, transform.position, Quaternion.identity);
            StartCoroutine("ReturnBomb");
        }
    }

    private void FixedUpdate()
    {
        //自機がカメラ外に行かないような諸々
        var position = this.rigidbody2D.position;
        var velocity = this.dir;

        //左シフトを押している間は低速移動
        if (Input.GetKey(KeyCode.LeftShift))
        {
           velocity = this.dir * this.SlowSpeed;
        }
        else
        {
           velocity = this.dir * this.NormalSpeed;
        }

        var deltaPosition = velocity * Time.deltaTime;

        var nextPosition = position + deltaPosition;

        var clampedNextPosition = new Vector2(Mathf.Clamp(nextPosition.x,this.boundL,this.boundR),Mathf.Clamp(nextPosition.y, this.boundB, this.boundT));

        var clampedDeltaPosition = clampedNextPosition - position;

        var clampedVelocity = clampedDeltaPosition / Time.deltaTime;

        this.rigidbody2D.velocity = clampedVelocity;
    }

    //自機と敵，敵弾が衝突したときのこと
    private void OnTriggerEnter2D(Collider2D collision)
    {
        string layerName = LayerMask.LayerToName(collision.gameObject.layer);

        if (layerName == "Enemy" || layerName == "Enemy_Bullet")
        {
            audioSource.PlayOneShot(damageClip);
            //レイヤを切り替える
            gameObject.layer = LayerMask.NameToLayer("PlayerDamage");
            //敵と衝突したとき
            if (layerName == "Enemy")
            {
                damage = 2;
                StartCoroutine("Damage");

            }
            //敵弾と衝突したとき
            if (layerName == "Enemy_Bullet")
            {
                damage = collision.gameObject.GetComponent<EnemyBulletController>().Damage;
                StartCoroutine("Damage");

                Destroy(collision.gameObject);
            }
        }
    }
    //ダメージを受けた時のこと
    IEnumerator Damage()
    {   
        // ダメージを受ける
        hp -= damage;
        Debug.Log("now HP : " + hp);


        //無敵時間
        int count = 0;
        //自機を点滅させる
        while (count < 6)
        {
            GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0);

            yield return new WaitForSeconds(0.2f);

            GetComponent<Renderer>().material.color = new Color(1, 1, 1, 1);

            yield return new WaitForSeconds(0.2f);
            count++;
        }
        //レイヤを戻す
        gameObject.layer = LayerMask.NameToLayer("Player");
    }

    IEnumerator ReturnBomb()
    {
        // ボムを撃てなくする
        isBomb = false;

        // 10秒待つ
        yield return new WaitForSeconds(10.0f);

        // 10秒たったのでまた撃てるようにする
        isBomb = true;
    }

    public void isGameOver()
    {
        gameOver = true;
    }
}
