﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombController : MonoBehaviour
{
    int frame;
    // Start is called before the first frame update
    void Start()
    {
        frame = 0;   
    }

    // Update is called once per frame
    void Update()
    {
        frame++;
        //その場にボムのオブジェクト生成
        transform.Translate(0, 0, 0);

        //50フレーム進んだら消す
        if (frame >= 50)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        string layerName = LayerMask.LayerToName(collision.gameObject.layer);
        //敵弾と衝突したら敵弾を消す
        if (layerName == "Enemy_Bullet")
        {
            Destroy(collision.gameObject);
        }
    }
}
