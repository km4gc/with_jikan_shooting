﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    private float boundT;
    // Start is called before the first frame update
    void Start()
    {
        var mainCamera = Camera.main;
        var positionZ = this.transform.position.z;
        var topRight = mainCamera.ViewportToWorldPoint(new Vector3(1.0f, 1.0f, positionZ));
        
        boundT = topRight.y;
        

    }

    // Update is called once per frame
    void Update()
    {
        //自弾をy軸方向に発射
        transform.Translate(0, 0.4f, 0);

        //カメラ外に出たら弾を消去
        if (transform.position.y > boundT) 
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //敵と衝突したら
        if (!collision.name.Contains("Player"))
        {
            //点数を加算
            GameObject.Find("Canvas").GetComponent<UIController>().KillScore();
            //弾は消す
            Destroy(gameObject);
        }
    }

}
