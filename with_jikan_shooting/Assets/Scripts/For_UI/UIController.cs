﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{

    int score = 0;
    GameObject scoreText;
    public Text gameOverText;
    public Text backText;

    private PlayerController player;
    public void KillScore()
    {
        this.score += 100;
    }

    public void AddScore()
    {
        this.score += 1;
    }

    public void GameOver()
    {
        //ゲームオーバーになったら表示
        gameOverText.enabled = true;
        backText.enabled = true;
    }
    // Start is called before the first frame update
    void Start()
    {
        this.scoreText = GameObject.Find("Score");
        //ゲームオーバーとかのテキストは初めは非表示
        gameOverText.enabled = false;
        backText.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.GetComponent<Text>().text = "Score:" + score.ToString("D10");

    }
}
