﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPBarController : MonoBehaviour
{
    private PlayerController player;
    public Slider slider;
    private float maxhp;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        slider = GameObject.Find("Slider").GetComponent<Slider>();
        maxhp = player.maxhp;
        Debug.Log("HP : " + maxhp);
    }

    // Update is called once per frame
    void Update()
    {
        //スライダーの値はmaxhpと現hpの比（最大値1）
        slider.value = player.hp / maxhp;
    }
}
