﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToTitle : MonoBehaviour
{
    private GameObject Player;
    private GameObject Spawner;
    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("Player");
        Spawner = GameObject.Find("Test_Stage_Spawner");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene("Title_scene");
        }
        //Playerオブジェクトがなくなったとき起動
        if(Player == null)
        {   
            //スペースキーでタイトルに戻る
            if (Input.GetKey(KeyCode.Space))
            {
                SceneManager.LoadScene("Title_scene");
            }
        }
    }
}
