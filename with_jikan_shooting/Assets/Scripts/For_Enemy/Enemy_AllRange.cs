﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_AllRange : Base_Enemy
{
    // 全方位弾が広がる中心
    private Vector2 center;
    // 全方位弾の角度間隔
    public float bullet_angle_interval;
    // 全方位弾の半径の広がる速度
    public float bullet_radius_speed;
    // 全方位弾の回転速度
    public float bullet_angle_speed;
    // 発射点の回転スピード
    public float launcher_angle_speed;

    // 敵弾のリスト
    public GameObject[] bullet_list;

    // 敵弾発射処理
    private float launch_ang; // 変数 : 発射台の角度制御
    // 弾を作成する処理
    public override void Shoot(Transform origin)
    {
        float ang;
        for(ang = 0f; ang < 360f; ang += bullet_angle_interval)
        {
            // 発射方向を回転
            origin.rotation = Quaternion.AngleAxis(launch_ang + ang, new Vector3(0, 0, 1));

            // bullet_listの弾を一通り生成
            for(int j = 0; j < bullet_list.Length; j++)
            {
                // 弾を引数 origin と同じ位置/角度で作成
                var child = Instantiate(bullet_list[j], origin.position, origin.rotation);
                // 攻撃力を設定
                var setobj = child.GetComponent<E_RoundBullet>();
                setobj.setDamege(m_damage);
                setobj.setCenter(origin.position);
                setobj.setAngleSpeed(bullet_angle_speed);
                setobj.setRadiusSpeed(bullet_radius_speed);
            }
            
        }
    }

    // 弾を発射するためのコルーチン
    IEnumerator Shot()
    {
        // 最初のショット前には0.5秒まつ
        yield return new WaitForSeconds(0.5f);

        launch_ang = 0f;
        while (true)
        {

            // 残りショット数 : 初期化
            int shot_left = shotTimes;
            while (shot_left > 0)
            {
                // 子要素を全て取得する
                for (int i = 0; i < transform.childCount; i++)
                {
                    // Transformコンポーネント shotPosition を作成して子要素を格納
                    Transform shotPosition = transform.GetChild(i);
                    if (shotPosition.name.Contains("Bullet_point"))
                    {
                        // 弾を発射台と同じ位置/角度で作成
                        this.Shoot(shotPosition);
                    }
                }
                // shotDelay 秒待つ
                yield return new WaitForSeconds(this.shotDelay);

                // 発射回数をデクリメント
                shot_left--;
                // 発射台回転角度を増加
                launch_ang += launcher_angle_speed;
                if(launch_ang > 360f)
                {
                    launch_ang -= 360f;
                }
            }

            yield return new WaitForSeconds(this.GroupDelay);
        }

    }

}
