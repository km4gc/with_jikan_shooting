﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ATTACK_TYPE
{
    FREE, // 発射方向指定
    LOCKON, // 自機狙い
    ALLRANGE, // 全方位
    TORNADE, // 渦巻弾
    R_TORNADE, // 渦巻弾(逆回り)
    SIZEOF, // 要素数
}


public class Enemy_Boss : Base_Enemy
{
    [Header("BOSS 射撃")]
    // リスト : bullet
    public GameObject[] bullet_list;

    // リスト : 発射台の攻撃パターン
    public ATTACK_TYPE[] attack_list;
    // リスト : 各攻撃に使う敵弾
    public int[] attack_bullet;
    // 変数 : 攻撃リストを逐次実行するかどうか
    public bool isSerial;
    // 各攻撃種毎ののインターバルリスト
    public float[] l_GroupDelay;
    // 各攻撃内での発射インターバル
    public float[] l_shotDelay;
    // 各攻撃の発射数
    public int[] l_shotTimes;
    // ボスのレベル
    private int level;
    // 難易度を上げる係数 : インターバル時間
    [Range(0.8f, 1f)]
    public float timeAccRate;
    private float nowRate = 1.0f;
    // 難易度を上げる係数 : 弾の増え方
    public int bulletPlusAmount;

    // ロックオン発射台のインデックスリスト
    private List<int> l_lockon = new List<int>();
    // ダメージアクセス用リスト
    private List<int> l_damage = new List<int>();
    // 半径速度アクセス用リスト
    private List<float> l_rad_spd = new List<float>();
    // 回転速度アクセス用リスト
    private List<float> l_ang_spd = new List<float>();
    // 全方位弾・渦巻弾 角度制御変数アクセス用リスト
    private List<float> l_ang = new List<float>();


    // --- 自機狙い用変数 ---

    // プレイヤーを格納
    private GameObject target;

    // ----------------------

    // --- 全方位弾用変数 ---
      
    // 全方位弾が広がる中心
    private Vector2 center;
    // 全方位弾の角度間隔
    public float bullet_angle_interval;
    // 全方位弾の半径の広がる速度
    public float bullet_radius_speed;
    // 全方位弾の回転速度
    public float bullet_angle_speed;
    // 発射点の回転スピード
    public float launcher_angle_speed;

    // ----------------------

    // --- 渦巻弾用変数 ---

    // 渦巻弾の弾ごとの角度間隔
    public float tornade_angle_interval;

    // --------------------

    // Start is called before the first frame update
    void Start()
    {
        Spawner = GameObject.FindGameObjectWithTag("Spawner");
        // HP を初期化する
        m_hp = m_hpMax;
        // 難易度初期化
        nowRate = 1.0f;
        // 移動先のindexを初期化
        moveIndex = 0;
        // 弾の発射処理（コルーチン Shot ）を実行
        if (isSerial)
        {
            StartCoroutine("ShotSerial");
        }
        else
        {
            for(int i = 0; i < attack_list.Length; i++)
            {
                l_damage.Add(m_damage);
                l_rad_spd.Add(bullet_radius_speed);
                l_ang_spd.Add(bullet_angle_speed);
                l_ang.Add(0f);
                StartCoroutine(ShotParallel(i));
            }
        }

        // 自機オブジェクトを検索
        target = GameObject.Find("Player");
        // ロックオンの発射台を検索
        for(int i = 0; i < attack_list.Length; i++)
        {
            if(attack_list[i] == ATTACK_TYPE.LOCKON)
            {
                l_lockon.Add(i);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        c_stop_timer += Time.deltaTime;

        // MOVE_TYPEがFREE,BOSSなら移動インターバルを制御
        if (m_moveType == MOVE_TYPE.FREE || m_moveType == MOVE_TYPE.BOSS)
        {
            // 止まった時だけ攻撃する場合 : コルーチンの停止処理を挟む
            if (isAttackByStop)
            {
                if (isMoveEnd)
                {
                    if (!isRunningCoroutine)
                    {
                        StartCoroutine("Shot");
                        isRunningCoroutine = true;
                    }
                    if (timer < move_interval)
                    {
                        timer += Time.deltaTime;
                        return;
                    }
                    else
                    {
                        isMoveEnd = false;
                        timer = 0f;
                    }
                }
                else
                {
                    if (isRunningCoroutine)
                    {
                        StopCoroutine("Shot");
                        isRunningCoroutine = false;
                    }
                }
            }
            // 動いてるときも攻撃するとき : 移動だけ制御
            else
            {
                if (isMoveEnd)
                {
                    if (timer < move_interval)
                    {
                        timer += Time.deltaTime;
                        return;
                    }
                    else
                    {
                        isMoveEnd = false;
                        timer = 0f;
                    }
                }
            }
        }

        // 移動用関数呼び出し
        Move(m_moveType);

        if (target != null)
        {
            for(int i = 0; i < l_lockon.Count; i++)
            {
                // 自機と敵機の位置から向くべき方向を計算し、Rotationを操作
                Vector3 diff = (target.transform.position
                    - transform.GetChild(l_lockon[i]).position).normalized;
                transform.GetChild(l_lockon[i]).rotation = Quaternion.FromToRotation(Vector3.up, diff);
            }
        }
    }

    // 敵の攻撃パターンを参照するインデックス
    private int index = 0;

    public void FreeShoot(Transform origin)
    {
        // 弾を引数 origin と同じ位置/角度で作成
        var child = Instantiate(bullet_list[attack_bullet[index]], origin.position, origin.rotation);
        // 攻撃力を設定
        child.GetComponent<EnemyBulletController>().setDamege(m_damage);
    }
    public void FreeShoot(Transform origin, int i)
    {
        // 弾を引数 origin と同じ位置/角度で作成
        var child = Instantiate(bullet_list[attack_bullet[i]], origin.position, origin.rotation);
        // 攻撃力を設定
        child.GetComponent<EnemyBulletController>().setDamege(l_damage[i]);
    }

    private float launch_ang; // 変数 : 発射台の角度制御
    public void AllRangeShoot(Transform origin)
    {
        float ang;
        for (ang = 0f; ang < 360f; ang += bullet_angle_interval)
        {
            // 発射方向を回転
            origin.rotation = Quaternion.AngleAxis(launch_ang + ang, new Vector3(0, 0, 1));

            // 弾を引数 origin と同じ位置/角度で作成
            var child = Instantiate(bullet_list[attack_bullet[index]], origin.position, origin.rotation);
            // 敵弾の各種パラメータを設定
            var setobj = child.GetComponent<E_RoundBullet>();
            setobj.setDamege(m_damage);
            setobj.setCenter(origin.position);
            setobj.setAngleSpeed(bullet_angle_speed);
            setobj.setRadiusSpeed(bullet_radius_speed);
        }
    }
    public void AllRangeShoot(Transform origin, int i)
    {
        float ang;
        for (ang = 0f; ang < 360f; ang += bullet_angle_interval)
        {
            // 発射方向を回転
            origin.rotation = Quaternion.AngleAxis(l_ang[i] + ang, new Vector3(0, 0, 1));

            // 弾を引数 origin と同じ位置/角度で作成
            var child = Instantiate(bullet_list[attack_bullet[i]], origin.position, origin.rotation);
            // 敵弾の各種パラメータを設定
            var setobj = child.GetComponent<E_RoundBullet>();
            setobj.setDamege(l_damage[i]);
            setobj.setCenter(origin.position);
            setobj.setAngleSpeed(l_ang_spd[i]);
            setobj.setRadiusSpeed(l_rad_spd[i]);
        }
    }

    // 渦巻弾用発射角度調整変数
    private float tr_launch_ang;
    public void TornadeShot(Transform origin, bool isR)
    {
        var ang = tr_launch_ang;
        if (isR)
        {
            ang *= -1f;
        }
        // 角度間隔だけ発射方向を補正
        // 発射方向を回転
        origin.rotation = Quaternion.AngleAxis(ang, new Vector3(0, 0, 1));

        // 弾を引数 origin と同じ位置/角度で作成
        var child = Instantiate(bullet_list[attack_bullet[index]], origin.position, origin.rotation);
        // 敵弾の各種パラメータを設定
        var setobj = child.GetComponent<E_RoundBullet>();
        setobj.setDamege(m_damage);
        setobj.setCenter(origin.position);
        setobj.setAngleSpeed(bullet_angle_speed);
        setobj.setRadiusSpeed(bullet_radius_speed);
    }
    public void TornadeShot(Transform origin, bool isR, int i)
    {
        var ang = l_ang[i];
        if (isR)
        {
            ang *= -1f;
        }
        // 角度間隔だけ発射方向を補正
        // 発射方向を回転
        origin.rotation = Quaternion.AngleAxis(ang, new Vector3(0, 0, 1));

        // 弾を引数 origin と同じ位置/角度で作成
        var child = Instantiate(bullet_list[attack_bullet[i]], origin.position, origin.rotation);
        // 敵弾の各種パラメータを設定
        var setobj = child.GetComponent<E_RoundBullet>();
        setobj.setDamege(l_damage[i]);
        setobj.setCenter(origin.position);
        setobj.setAngleSpeed(l_ang_spd[i]);
        setobj.setRadiusSpeed(l_rad_spd[i]);
    }

    IEnumerator ShotSerial()
    {
        // 発射角度制御変数初期化
        launch_ang = 0f;
        tr_launch_ang = 0f;

        while (true)
        {
            yield return new WaitForSeconds(l_GroupDelay[index]);

            // 残りショット数 : 初期化
            int shot_left = l_shotTimes[index];

            // Transformコンポーネント shotPosition を作成して子要素を格納
            Transform shotPosition = transform.GetChild(index);

            while (shot_left > 0)
            {
                // 弾をプレイヤーと同じ位置/角度で作成
                if(attack_list[index] == ATTACK_TYPE.FREE 
                    || attack_list[index] == ATTACK_TYPE.LOCKON)
                {
                    for(int i = 0; i < shotPosition.transform.childCount; i++)
                    {
                        // 発射台の子になっているオブジェクト方向に弾生成
                        FreeShoot(shotPosition.GetChild(i));
                    }
                }
                else if(attack_list[index] == ATTACK_TYPE.ALLRANGE)
                {
                    AllRangeShoot(shotPosition);
                    // 発射台回転角度を増加
                    launch_ang += launcher_angle_speed;
                    if (launch_ang > 360f)
                    {
                        launch_ang -= 360f;
                    }
                }
                else if(attack_list[index] == ATTACK_TYPE.TORNADE 
                    || attack_list[index] == ATTACK_TYPE.R_TORNADE)
                {
                    TornadeShot(shotPosition, attack_list[index] == ATTACK_TYPE.R_TORNADE);
                    // 発射台回転角度を増加
                    tr_launch_ang += tornade_angle_interval;
                    if (tr_launch_ang > 360f)
                    {
                        tr_launch_ang -= 360f;
                    }
                }
                

                // shotDelay 秒待つ
                yield return new WaitForSeconds(l_shotDelay[index]);

                // 発射回数をデクリメント
                shot_left--;
            }

            // 攻撃参照インデックスを進める
            index++;
            if(index >= attack_list.Length)
            {
                index = 0;
            }
        }
    }

    IEnumerator ShotParallel(int i_num)
    {
        // Debug.Log("para_ind : " + i_num);

        // 最初のショット前には0.5秒まつ
        yield return new WaitForSeconds(0.5f);

        // 発射角度制御変数初期化
        launch_ang = 0f;
        tr_launch_ang = 0f;

        // Transformコンポーネント shotPosition を作成して子要素を格納
        Transform shotPosition = transform.GetChild(i_num);

        while (true)
        {
            // 残りショット数 : 初期化
            int shot_left = l_shotTimes[i_num];

            while (shot_left > 0)
            {
                // 弾をプレイヤーと同じ位置/角度で作成
                if (attack_list[i_num] == ATTACK_TYPE.FREE 
                    || attack_list[i_num] == ATTACK_TYPE.LOCKON)
                {
                    for (int i = 0; i < shotPosition.transform.childCount; i++)
                    {
                        // 発射台の子になっているオブジェクト方向に弾生成
                        FreeShoot(shotPosition.GetChild(i), i_num);
                    }
                }
                else if (attack_list[i_num] == ATTACK_TYPE.ALLRANGE)
                {
                    AllRangeShoot(shotPosition, i_num);
                    // 発射台回転角度を増加
                    l_ang[i_num] += launcher_angle_speed;
                    if (l_ang[i_num] > 360f)
                    {
                        l_ang[i_num] -= 360f;
                    }
                }
                else if (attack_list[i_num] == ATTACK_TYPE.TORNADE 
                    || attack_list[i_num] == ATTACK_TYPE.R_TORNADE)
                {
                    TornadeShot(shotPosition, 
                        attack_list[i_num] == ATTACK_TYPE.R_TORNADE,
                        i_num);
                    // 発射台回転角度を増加
                    l_ang[i_num] += tornade_angle_interval;
                    if (l_ang[i_num] > 360f)
                    {
                        l_ang[i_num] -= 360f;
                    }
                }


                // shotDelay 秒待つ
                yield return new WaitForSeconds(l_shotDelay[i_num]);

                // 発射回数をデクリメント
                shot_left--;
            }

            yield return new WaitForSeconds(l_GroupDelay[i_num]);
        }
    }

    public void LevelUP(int l)
    {
        level = l;
        Debug.Log(nowRate);
        for (int i = 0; i < l - 1; i++) nowRate *= timeAccRate;
        Debug.Log(nowRate);
        SetDifficult();
    }

    private void SetDifficult()
    {
        for(int i = 0; i < l_shotTimes.Length; i++)
        {
            if(attack_list[i] == ATTACK_TYPE.TORNADE
                || attack_list[i] == ATTACK_TYPE.TORNADE)
            {
                l_shotTimes[i] += bulletPlusAmount * (int)(360f / tornade_angle_interval);
            }
            else
            {
                l_shotTimes[i] += bulletPlusAmount;
            }
        }
        for(int i = 0; i < l_GroupDelay.Length; i++)
        {
            l_GroupDelay[i] *= nowRate;
        }
    }

    // 他のオブジェクトと衝突した時に呼び出される関数
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // 即死を防ぐ
        if (c_stop_timer < 0.1f) return;

        // 弾と衝突した場合
        if (collision.name.Contains("BulletPrefab"))
        {
            // Debug.Log("Hit");

            // 弾を削除する
            Destroy(collision.gameObject);

            // 敵の HP を減らす
            m_hp--;

            // 敵の HP がまだ残っている場合はここで処理を終える
            if (0 < m_hp) return;

            // 敵残数を減らす
            Spawner.GetComponent<Test_Stage_Spawner>().e_amount--;
            // 撃破アニメの再生
            var Anime = Instantiate(defeat_anime, transform.position, transform.rotation);
            // 敵を削除する
            Destroy(gameObject);
        }
    }
}
