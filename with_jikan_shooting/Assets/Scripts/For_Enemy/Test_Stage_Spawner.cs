﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_Stage_Spawner : MonoBehaviour
{
    [Header("ゲームレベル")]
    [Range(0.8f, 1f)]
    public float accelerateRate;

    public float break_time = 1.0f; // ボス後の休憩時間

    [Header("敵リスト")]
    public Base_Enemy[] m_enemyPrefabs; // 敵のプレハブを管理する配列

    [System.NonSerialized]public int team_amount; //チーム数

    public int[] enemy_amount; // 編隊内の敵機数
    public float[] team_interval; // 編隊間の出現間隔 (sec.)
    public float[] interval; // 編隊内での出現間隔 (sec.) *同時出現の場合は0、float値は1体ずつ出現*
    public int[] stage_enemies; // 編隊で使う敵機の種類
    public RESPAWN_TYPE[] team_respownType; // 編隊の出現タイプ
    public MOVE_TYPE[] team_moveType; // 編隊の軌道タイプ 
    public Vector2[] leader_pos; // 編隊内基準機体の位置
    public Vector2[] team_pos_gap; // 編隊内敵機間の距離 *等距離*

    int index = 0; // 参照用変数 : 各種配列のインデックス
    int m_enemy_amount; // 参照用変数 : 編隊内の敵機数
    float m_team_interval; // 参照用変数 : 編隊間の出現間隔 (sec.)
    float m_interval; // 参照用変数 : 編隊内での出現間隔 (sec.)
    int enemyIndex; // 参照用変数 : 敵Prefab
    Vector2 pos; // 参照用変数 : 編隊内基準機体の位置
    Vector2 gap; // 参照用変数 : 編隊内敵機間の距離
    RESPAWN_TYPE respawnType; // 参照用変数 : 出現タイプ
    MOVE_TYPE moveType; // 参照用変数 : 敵機の移動パターン

    private float m_timer; // 出現タイミングを管理するタイマー
    private float m_team_timer; // 編隊の出現タイミングを管理するタイマー
    private float m_break_timer; // ボス後の休憩時間タイマー

    [System.NonSerialized]public int e_amount; // 敵の数を管理
    private bool isSpawning; // 敵の生成が途中かどうか
    private bool isEnd; // 編隊リストが終了したかどうか
    private int enemy_left; // 生成中の敵の残り
    private bool isBoss = false; // ボスが出現中かどうか
    private bool isDefeatedBoss = false; // ボスが倒された直後かどうか
    private int loop; // 現在の周回数
    private float nowLevelRate;

    public GameObject BGM_Mixer;
    private BGM_Mixer mix;
    private bool isBossBGM = false;

    // Start is called before the first frame update
    void Start()
    {
        mix = BGM_Mixer.GetComponent<BGM_Mixer>();
        mix.Play();

        index = 0;
        e_amount = 0;
        isSpawning = false;
        isEnd = false;
        team_amount = enemy_amount.Length;
        loop = 1;
        nowLevelRate = 1f;
        SearchDatabase();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Debug.Log("E_amount : " + e_amount);

        if (isDefeatedBoss)
        {
            m_break_timer += Time.deltaTime;
            if (m_break_timer < break_time)
            {
                return;
            }
            else
            {
                isDefeatedBoss = false;
                m_break_timer = 0f;
                mix.ChangeBGM(isBoss);
                isBossBGM = false;
            }
        }

        // ボス中は全滅するまで出現を止める
        if (isBoss)
        {
            if (!isBossBGM)
            {
                mix.ChangeBGM(isBoss);
                isBossBGM = true;
            }

            if (e_amount != 0 && !isSpawning)
            {
                return;
            }
            else if (e_amount == 0 && !isSpawning)
            {
                m_team_timer = 0f;
                isBoss = false;
                isDefeatedBoss = true;
                return;
            }
        }

        if (isEnd && !isSpawning)
        {
            loop++;
            nowLevelRate *= accelerateRate;
            index = 0;
            SearchDatabase();
            isEnd = false;
            return;
        }

        // 出現タイミングを管理するタイマーを更新する
        m_team_timer += Time.deltaTime;
        if (m_team_timer >= m_team_interval)
        {
            // 編隊生成に入っていれば、敵機個別の出現管理タイマー更新
            m_timer += Time.deltaTime;
        }

        // 編隊全滅 or 待ち時間経過するまでは、
        // このタイミングでフレーム処理終了
        if (e_amount != 0 && m_team_timer < m_team_interval) return;

        // 出現する敵のプレハブを取得
        var enemyPrefab = m_enemyPrefabs[enemyIndex];
        // 生成開始をフラグ管理する
        if (!isSpawning)
        {
            Debug.Log("Now Team : " + index);

            isSpawning = true;
            // 1体ずつ出現の場合、残り敵機数を管理
            enemy_left = m_enemy_amount;
            // 最初の1体はすぐに出現
            m_timer = m_interval;

            //Debug.Log("enemy_left : " + enemy_left);
        }

        // 同時出現の場合
        if (m_interval == 0f)
        {
            for(int i = 0; i < m_enemy_amount; i++)
            {
                // 敵のゲームオブジェクトを生成する
                var enemy = Instantiate(enemyPrefab);
                // 敵を初期化する
                enemy.Init(respawnType, pos + i * gap);
                // 敵の動きを初期化
                enemy.MoveInit(moveType);
                // 敵機数に加算
                e_amount++;

                // 現在の出現がボスかどうか
                isBoss = (moveType == MOVE_TYPE.BOSS);
                // ボスならレベルを設定
                if (isBoss) enemy.GetComponent<Enemy_Boss>().LevelUP(loop);
            }
            
            // 編隊生成タイマーをリセット
            m_team_timer = 0f;
            // 生成終了のフラグ管理
            isSpawning = false;
            // まだ編隊が残っていればサーチを進める
            if (index < team_amount) SearchDatabase();
            else
            {
                isEnd = true;
            }
        }
        // 1機ずつ出現の場合
        else
        {
            // 編隊生成中は始めのif文を通過するようにする
            if(enemy_left > 0) m_team_timer = m_team_interval;

            // 敵個体出現間隔が経過
            // 生成開始
            if (m_timer < m_interval) return;
            // 敵個体生成タイマーリセット
            m_timer = 0f;

            // 敵機数に加算
            e_amount++;
            // 敵のゲームオブジェクトを生成する
            var enemy = Instantiate(enemyPrefab);
            // 敵を初期化する
            enemy.Init(respawnType, pos + (m_enemy_amount - enemy_left) * gap);
            // 敵の動きを初期化
            enemy.MoveInit(moveType);
            // 残り敵機数を減少させる
            enemy_left--;

            // 現在の出現がボスかどうか
            isBoss = (moveType == MOVE_TYPE.BOSS);
            // ボスならレベルを設定
            if (isBoss) enemy.GetComponent<Enemy_Boss>().LevelUP(loop);

            // 編隊生成完了 かつ まだ編隊が残っていれば
            if (enemy_left == 0 && index < team_amount)
            {
                // 編隊生成タイマーのリセット
                m_team_timer = 0;
                // 生成終了のフラグ管理
                isSpawning = false;
                // サーチを進める
                SearchDatabase();
            }
            else if(enemy_left == 0)
            {
                // 編隊生成タイマーのリセット
                m_team_timer = 0;
                // 生成終了のフラグ管理
                isSpawning = false;
                isEnd = true;
            }
        }
    }

    // 編隊情報の読み込み
    private void SearchDatabase()
    {
        // Debug.Log("Now Team : " + index);

        enemyIndex = stage_enemies[index];
        m_enemy_amount = enemy_amount[index];
        m_team_interval = team_interval[index];
        m_interval = interval[index];
        pos = leader_pos[index];
        gap = team_pos_gap[index];
        respawnType = team_respownType[index];
        moveType = team_moveType[index];

        // ボス以外は周回によってレベル調整
        if(moveType != MOVE_TYPE.BOSS)
        {
            m_enemy_amount += loop / 2;
        }
        m_team_interval *= nowLevelRate;
        m_interval *= nowLevelRate;


        // データサーチ用インデックスを次に進める;
        index++;
    }

}
