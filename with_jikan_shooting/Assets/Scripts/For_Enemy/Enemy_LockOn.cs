﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_LockOn : Base_Enemy
{
    // プレイヤーを格納
    private GameObject target;

    // Start is called before the first frame update
    void Start()
    {
        Spawner = GameObject.FindGameObjectWithTag("Spawner");
        // HP を初期化する
        m_hp = m_hpMax;
        // 移動先のindexを初期化
        moveIndex = 0;
        // 弾の発射処理（コルーチン Shot ）を実行
        StartCoroutine("Shot");
        
        // 自機オブジェクトを検索
        target = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        c_stop_timer += Time.deltaTime;

        // MOVE_TYPEがFREE,BOSSなら移動インターバルを制御
        if (m_moveType == MOVE_TYPE.FREE || m_moveType == MOVE_TYPE.BOSS)
        {
            if (isMoveEnd)
            {
                if (timer < move_interval)
                {
                    timer += Time.deltaTime;
                    return;
                }
                else
                {
                    isMoveEnd = false;
                    timer = 0f;
                }
            }
        }

        // 移動用関数呼び出し
        Move(m_moveType);

        if(target != null)
        {
            // 自機と敵機の位置から向くべき方向を計算し、Rotationを操作
            Vector3 diff = (this.target.transform.position - this.transform.position).normalized;
            this.transform.rotation = Quaternion.FromToRotation(Vector3.up, diff);
        }
    }
}
