﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 敵の出現位置の種類
public enum RESPAWN_TYPE
{
    UP, // 上
    RIGHT, // 右
    DOWN, // 下
    LEFT, // 左
    SIZEOF, // 敵の出現位置の数
}

// 敵の軌道の種類
public enum MOVE_TYPE
{
    STRAIGHT, // まっすぐ
    CIRCLE, // 円軌道【時計回り】
    R_CIRCLE, // 円軌道【逆時計回り】
    FREE, // 自由軌道
    BOSS, // ボス軌道
    SIZEOF, // 敵の軌道の種類の数
}

// 敵の制御コンポーネント原型 : 抽象クラス
public class Base_Enemy : MonoBehaviour
{
    [Header("出現位置")]
    public Vector2 m_respawnPosInside; // 敵の出現位置（内側）
    public Vector2 m_respawnPosOutside = new Vector2(4.5f, 6.5f); // 敵の出現位置（外側）

    [Header("動き")]
    public MOVE_TYPE m_moveType; // 移動軌道の種類
    public float m_speed = 0.05f; // 移動する速さ
    public Vector2 m_center; // 円軌道の中心点
    public float m_angle; // 円移動角度
    // MOVE_TYPEがFREE,BOSSの時に移動先を指定
    public Vector2[] movepoint;
    // MOVE_TYPEがFREE,BOSSの時に移動と移動の間隔を指定
    public float move_interval;
    // 目的地と自分の距離の閾値 --> 下回ったら到着とする
    public float arrive_distance = 0.1f;
    // 公転移動の際、rotateをやめるまでの時間
    public float stop_circle_timer = 10.0f;

    [Header("HP・ダメージ")]
    public int m_hpMax; // HP の最大値
    public int m_damage; // この敵がプレイヤーに与えるダメージ

    [System.NonSerialized] public int m_hp; // HP
    private Vector3 m_direction; // 進行方向

    [Header("射撃")]
    // 弾を撃つ間隔を格納する変数
    public float shotDelay;
    // 弾のグループを打つ間隔を格納する変数
    public float GroupDelay;
    // 弾のグループにおける発射数
    public int shotTimes;
    // 弾のプレハブを格納する変数
    public GameObject bullet;
    // 変数 : 止まった時だけ攻撃するかどうか
    public bool isAttackByStop = false;

    [Header("依存ゲームオブジェクト")]
    // 変数 : 敵の出現管理をするオブジェクトを格納
    public GameObject Spawner;
    // 撃破時に出すアニメーション
    public GameObject defeat_anime;

    // 変数 : 移動完了を検知
    [System.NonSerialized] public bool isMoveEnd = false;
    // 変数 : コルーチンが動いてるかを検知
    [System.NonSerialized] public bool isRunningCoroutine = true;
    // 変数 : 指定ポイントがもうないことを検知
    [System.NonSerialized] public bool isPointEnd = false;
    // 変数 : 複数ある移動先を参照するindex
    [System.NonSerialized] public int moveIndex;
    // 変数 : タイマー
    [System.NonSerialized] public float timer = 0f;
    // 変数 : 公転ストップを測るタイマー
    [System.NonSerialized] public float c_stop_timer = 0f;



    // 敵が生成された時に呼び出される関数
    private void Start()
    {
        Spawner = GameObject.FindGameObjectWithTag("Spawner");
        // HP を初期化する
        m_hp = m_hpMax;
        // 移動先のindexを初期化
        moveIndex = 0;
        // 弾の発射処理（コルーチン Shot ）を実行
        StartCoroutine("Shot");
    }

    // 毎フレーム呼び出される関数
    private void Update()
    {
        // Debug.Log(c_stop_timer);
        c_stop_timer += Time.deltaTime;

        // MOVE_TYPEがFREE,BOSSなら移動インターバルを制御
        if (m_moveType == MOVE_TYPE.FREE || m_moveType == MOVE_TYPE.BOSS)
        {
            // 止まった時だけ攻撃する場合 : コルーチンの停止処理を挟む
            if (isAttackByStop)
            {
                if (isMoveEnd)
                {
                    if (!isRunningCoroutine)
                    {
                        StartCoroutine("Shot");
                        isRunningCoroutine = true;
                    }
                    if (timer < move_interval)
                    {
                        timer += Time.deltaTime;
                        return;
                    }
                    else
                    {
                        isMoveEnd = false;
                        timer = 0f;
                    }
                }
                else
                {
                    if (isRunningCoroutine)
                    {
                        StopCoroutine("Shot");
                        isRunningCoroutine = false;
                    }
                }
            }
            // 動いてるときも攻撃するとき : 移動だけ制御
            else
            {
                if (isMoveEnd)
                {
                    if (timer < move_interval)
                    {
                        timer += Time.deltaTime;
                        return;
                    }
                    else
                    {
                        isMoveEnd = false;
                        timer = 0f;
                    }
                }
            }
        }

        // 移動用関数呼び出し
        Move(m_moveType);
    }


    // 公転移動ストップ後の移動用変数
    private Vector3 toward;
    // 移動
    public void Move(MOVE_TYPE moveType)
    {
        switch (moveType)
        {
            // 直線軌道の場合
            case MOVE_TYPE.STRAIGHT:
                // 直線移動する
                transform.localPosition += m_direction * m_speed;
                break;
            // 円軌道の場合
            case MOVE_TYPE.CIRCLE:
            case MOVE_TYPE.R_CIRCLE:
                // 回転の中心をVector3型に --> RotateAround関数で使えるように
                Vector3 center = m_center;
                // 移動前座標
                Vector3 prev = transform.localPosition;
                // 移動先座標の準備
                Vector3 now;
                // 回転移動 : 中心-center, 回転軸-m_direction, 角度-m_speed
                if (c_stop_timer < stop_circle_timer)
                {
                    transform.RotateAround(center, m_direction, m_angle);
                    now = transform.localPosition;
                    toward = now - prev;
                }
                else
                {
                    transform.localPosition += toward.normalized * m_speed;
                }

                break;
            // 自由軌道の場合
            case MOVE_TYPE.FREE:
                // 直線移動する
                transform.localPosition += m_direction * m_speed;
                if (!isPointEnd)
                {
                    // ポイントに到着
                    if ((transform.localPosition - (Vector3)movepoint[moveIndex]).magnitude <= arrive_distance)
                    {
                        // 移動を待つためにフラグを立てる
                        isMoveEnd = true;
                        // 次の移動先を検索
                        moveIndex++;
                        if (moveIndex < movepoint.Length)
                        {
                            m_direction = ((Vector3)movepoint[moveIndex] - this.transform.position).normalized;
                        }
                        else
                        {
                            isPointEnd = true;
                        }
                    }
                }
                break;
            // ボス軌道の場合
            case MOVE_TYPE.BOSS:
                // 直線移動する
                transform.localPosition += m_direction * m_speed;
                // ポイントに到着
                if ((transform.localPosition - (Vector3)movepoint[moveIndex]).magnitude <= arrive_distance)
                {
                    // 移動を待つためにフラグを立てる
                    isMoveEnd = true;
                    // 次の移動先を検索
                    moveIndex++;
                    if (moveIndex >= movepoint.Length)
                    {
                        moveIndex = 0;
                    }
                    m_direction = ((Vector3)movepoint[moveIndex] - this.transform.position).normalized;
                }
                break;
        }
    }

    public void MoveInit(MOVE_TYPE moveType)
    {
        switch (moveType)
        {
            // 直線軌道の場合
            case MOVE_TYPE.STRAIGHT:
                m_moveType = MOVE_TYPE.STRAIGHT;
                break;

            // 円軌道【時計回り】の場合
            case MOVE_TYPE.CIRCLE:
                m_moveType = MOVE_TYPE.CIRCLE;
                // m_directionを回転軸に設定
                m_direction = Vector3.back;
                break;

            // 円軌道【逆時計回り】の場合
            case MOVE_TYPE.R_CIRCLE:
                m_moveType = MOVE_TYPE.R_CIRCLE;
                // m_directionを回転軸に設定
                m_direction = Vector3.forward;
                break;

            // FREE軌道の場合
            case MOVE_TYPE.FREE:
                m_moveType = MOVE_TYPE.FREE;
                // m_directionを最初の移動先に設定
                m_direction = ((Vector3)movepoint[moveIndex] - this.transform.position).normalized;
                break;
            // BOSS軌道の場合
            case MOVE_TYPE.BOSS:
                m_moveType = MOVE_TYPE.BOSS;
                // m_directionを最初の移動先に設定
                m_direction = ((Vector3)movepoint[moveIndex] - this.transform.position).normalized;
                break;
        }
    }

    // 敵が出現する時に初期化する関数
    public void Init(RESPAWN_TYPE respawnType, Vector2 initPos)
    {
        var pos = Vector3.zero;

        // 指定された出現位置の種類に応じて、
        // 出現位置と進行方向を決定する
        switch (respawnType)
        {
            // 出現位置が上の場合
            case RESPAWN_TYPE.UP:
                pos.x = initPos.x;
                pos.y = m_respawnPosOutside.y;
                m_direction = Vector2.down;
                break;

            // 出現位置が右の場合
            case RESPAWN_TYPE.RIGHT:
                pos.x = m_respawnPosOutside.x;
                pos.y = initPos.y;
                m_direction = Vector2.left;
                break;

            // 出現位置が下の場合
            case RESPAWN_TYPE.DOWN:
                pos.x = initPos.x;
                pos.y = -m_respawnPosOutside.y;
                m_direction = Vector2.up;
                break;

            // 出現位置が左の場合
            case RESPAWN_TYPE.LEFT:
                pos.x = -m_respawnPosOutside.x;
                pos.y = initPos.y;
                m_direction = Vector2.right;
                break;
        }

        // 位置を反映する
        transform.localPosition = pos;
    }

    // 敵弾発射処理
    // 弾を作成する処理
    public virtual void Shoot(Transform origin)
    {
        // 弾を引数 origin と同じ位置/角度で作成
        var child = Instantiate(bullet, origin.position, origin.rotation);
        // 攻撃力を設定
        child.GetComponent<EnemyBulletController>().setDamege(m_damage);
    }
    // 弾を発射するためのコルーチン
    IEnumerator Shot()
    {
        // 最初のショット前には0.5秒まつ
        yield return new WaitForSeconds(0.5f);

        while (true)
        {
            // 残りショット数 : 初期化
            int shot_left = shotTimes;
            while (shot_left > 0)
            {
                // 子要素を全て取得する
                for (int i = 0; i < transform.childCount; i++)
                {
                    // Transformコンポーネント shotPosition を作成して子要素を格納
                    Transform shotPosition = transform.GetChild(i);
                    // 弾をプレイヤーと同じ位置/角度で作成
                    this.Shoot(shotPosition);
                }
                // shotDelay 秒待つ
                yield return new WaitForSeconds(this.shotDelay);

                // 発射回数をデクリメント
                shot_left--;
            }

            yield return new WaitForSeconds(this.GroupDelay);
        }

    }

    // 他のオブジェクトと衝突した時に呼び出される関数
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // 即死を防ぐ
        if (c_stop_timer < 0.1f) return;

        // 弾と衝突した場合
        if (collision.name.Contains("BulletPrefab"))
        {
            // Debug.Log("Hit");

            // 弾を削除する
            Destroy(collision.gameObject);

            // 敵の HP を減らす
            m_hp--;

            // 敵の HP がまだ残っている場合はここで処理を終える
            if (0 < m_hp) return;

            // 敵残数を減らす
            Spawner.GetComponent<Test_Stage_Spawner>().e_amount--;
            // 撃破アニメの再生
            var Anime = Instantiate(defeat_anime, transform.position, transform.rotation);
            // 敵を削除する
            Destroy(gameObject);
        }
        // 画面外の枠と衝突した場合
        else if (collision.name.Contains("Limit"))
        {
            // 敵残数を減らす
            Spawner.GetComponent<Test_Stage_Spawner>().e_amount--;
            // 敵を削除する
            Destroy(gameObject);
        }
    }

}
