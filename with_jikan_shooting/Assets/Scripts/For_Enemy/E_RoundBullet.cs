﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E_RoundBullet : EnemyBulletController
{
    // 回転角の速度
    [System.NonSerialized]public float angle_speed;
    // 弾が広がる速さ
    [System.NonSerialized]public float radius_speed;
    // 回転中心
    private Vector3 center;

    // 回転方向を反転するかどうか
    [SerializeField] private bool isReverse = false;
    // 角速度を0にするまでの時間
    [SerializeField] private float RoundStopTime = 4.0f;
    // タイマー
    float timer = 0f;
    // 各速度0のフラグ
    bool isTimeOver = false;

    // Start is called before the first frame update
    void Start()
    {
        if (isReverse)
        {
            angle_speed *= -1f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, radius_speed, 0);
        // transform.RotateAround(center, transform.forward, angle_speed * Time.deltaTime);
        if(!isTimeOver)
        {
            timer += Time.deltaTime;
            transform.Rotate(0, 0, angle_speed * Time.deltaTime);
            if (timer > RoundStopTime) isTimeOver = true;
        }
    }

    // 回転中心をセット
    public void setCenter(Vector3 pos)
    {
        center = pos;
    }
    // 半径拡大速度をセット
    public void setRadiusSpeed(float v)
    {
        radius_speed = v;
    }
    // 角速度をセット
    public void setAngleSpeed(float omega)
    {
        angle_speed = omega;
    }
}
