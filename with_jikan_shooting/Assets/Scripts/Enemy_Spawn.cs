﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Spawn : MonoBehaviour
{
    public Enemy[] m_enemyPrefabs; // 敵のプレハブを管理する配列
    public float m_team_interval; // 編隊の出現間隔
    public float m_interval; // 出現間隔（秒）

    private float m_timer; // 出現タイミングを管理するタイマー
    private float m_team_timer; // 編隊の出現タイミングを管理するタイマー

    RESPAWN_TYPE respawnType; // 出現タイプ
    int enemyIndex; // 敵Prefabを参照するための変数

    private void Start()
    {
        SelectEnemy();
        m_timer = m_interval;
    }

    // 毎フレーム呼び出される関数
    private void Update()
    {
        // 出現タイミングを管理するタイマーを更新する
        m_timer += Time.deltaTime;
        m_team_timer += Time.deltaTime;

        // まだ敵が出現するタイミングではない場合、
        // このフレームの処理はここで終える
        if (m_timer < m_interval) return;

        // 出現タイミングを管理するタイマーをリセットする
        m_timer = 0;

        // 一定時間ごとに敵の種類と座標を決定
        if (m_team_timer >= m_team_interval)
        {
            // 編隊選択タイミングを選択する管理するタイマーをリセット
            m_team_timer = 0;
            SelectEnemy();
        }

        // 出現する敵のプレハブを配列から取得する
        var enemyPrefab = m_enemyPrefabs[enemyIndex];

        // 敵のゲームオブジェクトを生成する
        var enemy = Instantiate(enemyPrefab);

        // 敵を初期化する
        enemy.Init(respawnType);
    }

    private void SelectEnemy()
    {
        // 出現する敵をランダムに決定する
        enemyIndex = Random.Range(0, m_enemyPrefabs.Length);

        // 敵を画面外のどの位置に出現させるかランダムに決定する
        respawnType = (RESPAWN_TYPE)Random.Range(
            0, (int)RESPAWN_TYPE.SIZEOF);
    }
}
