﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGM_Mixer : MonoBehaviour
{
    [SerializeField]
    private AudioSource[] _audios;

    /// <summary>
    /// BGMの混ぜ具合。0ならSound1、1ならSound2になる
    /// </summary>
    [Range(0, 1)]
    public float _mixRate = 0;
    [Range(0, 0.1f)]
    public float change_speed = 0.05f;

    private GameObject Player;
    private bool isGameover;
    private float masterVol = 1.0f;

    public void Start()
    {
        Player = GameObject.Find("Player");
        isGameover = false;
    }


    public void Play()
    {
        Debug.Log("Play");
        _audios[0].Play();
        _audios[1].Play();
    }

    private void Update()
    {
        _audios[0].volume = (1f - _mixRate) * masterVol;
        _audios[1].volume = _mixRate * masterVol;

        if (Player == null)
        {
            if (!isGameover)
            {
                Stop();
                isGameover = true;
            }
        }
    }

    public void ChangeBGM(bool isBoss)
    {
        StartCoroutine(change(isBoss));
    }

    IEnumerator change(bool isBoss)
    {
        var endchange = false;

        while(!endchange)
        {
            if (isBoss)
            {
                _mixRate += change_speed;
            }
            else
            {
                _mixRate -= change_speed;
            }
            if (_mixRate > 1.0f)
            {
                _mixRate = 1.0f;
                endchange = true;
            }
            else if (_mixRate < 0f)
            {
                _mixRate = 0f;
                endchange = true;
            }

            yield return null;
        }      
    }

    public void Stop()
    {
        StartCoroutine(FadeOut());
    }

    IEnumerator FadeOut()
    {
        while(_audios[0].volume > 0f || _audios[1].volume > 0f)
        {
            masterVol -= 0.025f;

            if (_audios[0].volume < 0f) _audios[0].volume = 0f;
            if (_audios[1].volume < 0f) _audios[1].volume = 0f;

            yield return null;
        }

        _audios[0].Stop();
        _audios[1].Stop();
    }
}
