﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletController : MonoBehaviour
{
    // 弾速
    public float speed = 0.3f;
    // 弾の攻撃力
    [System.NonSerialized]public int Damage = 0;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, speed, 0);
    }

    // 弾のダメージを設定
    public void setDamege(int d)
    {
        Damage = d;
    }

    // 他のオブジェクトと衝突した時に呼び出される関数
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // 画面外の枠と衝突した場合
        if (collision.name.Contains("Limit"))
        {
            // 敵を削除する
            Destroy(gameObject);
        }
        else if (collision.name.Contains("Player"))
        {
            return;
            // Debug.Log("Damege! >> " + Damage);
        }
    }
}
