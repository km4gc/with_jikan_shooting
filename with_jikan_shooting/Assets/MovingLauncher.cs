﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingLauncher : MonoBehaviour
{
    Transform center;
    Vector3 axis;
    public bool isR = false;
    public float angle_speed = 0f;

    // Start is called before the first frame update
    void Start()
    {
        center = transform.parent;
        if (isR) axis = Vector3.back;
        else axis = Vector3.forward;
    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(
            center.position,
            axis,
            angle_speed * Time.deltaTime);
    }
}
